package net.ryu.itemfilter;

import net.ryu.itemfilter.commands.CmdFilter;
import net.ryu.itemfilter.listener.PlayerListener;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ItemFilterPlugin extends JavaPlugin {
    private List<UUID> filterList;

    @Override
    public void onEnable() {
        init();
        filterList = new ArrayList<>();
    }

    void registerListeners(Listener... listeners) {
        for (Listener listener : listeners)
            getServer().getPluginManager().registerEvents(listener, this);
    }

    void registerCommands() {
        getCommand("filter").setExecutor(new CmdFilter(this));
    }

    void init() { registerListeners(new PlayerListener()); registerCommands(); }

    @Override
    public void onDisable() {

    }

    public List<UUID> getFilterList() {
        return filterList;
    }
}
