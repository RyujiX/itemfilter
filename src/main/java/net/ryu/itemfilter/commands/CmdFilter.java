package net.ryu.itemfilter.commands;

import net.ryu.itemfilter.ItemFilterPlugin;
import net.ryu.itemfilter.utilities.Txt;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdFilter implements CommandExecutor {
    private ItemFilterPlugin plugin;
    public CmdFilter(ItemFilterPlugin plugin) { this.plugin = plugin; }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length >= 0) {
            switch (args.length) {
                case 1:
                    if (sender instanceof Player) {
                        Player player = (Player) sender;
                        if (args[0].equalsIgnoreCase("edit")) {
                            if (player.hasPermission("itemfilter.edit")) {

                                return true;
                            } else {
                                player.sendMessage(Txt.parse(
                                        "&8[&c&l*&8] &cSorry, but it doesn't seem as if you have permission for this command."
                                ));
                                return true;
                            }
                        } else if (args[0].equalsIgnoreCase("toggle")) {
                          if (player.hasPermission("itemfilter.toggle")) {
                              if (plugin.getFilterList().contains(player.getUniqueId())) {
                                  plugin.getFilterList().remove(player.getUniqueId());
                                  player.sendMessage(Txt.parse(
                                          "&8[&d&l*&8] &dYou have successfully &nDisabled&d the item filter.\n" +
                                                  " &7Use &n/filter toggle&7 once more to re-enable it."
                                  ));
                              } else {
                                  plugin.getFilterList().add(player.getUniqueId());
                                  player.sendMessage(Txt.parse(
                                          "&8[&d&l*&8] &dYou have successfully &nEnabled&d the item filter.\n" +
                                                  " &7Use &n/filter edit&7 to edit the items you will pickup."
                                  ));
                              }
                              return true;
                          } else {
                              player.sendMessage(Txt.parse(
                                      "&8[&c&l*&8] &cSorry, but it doesn't seem as if you have permission for this command."
                              ));
                              return true;
                          }
                        }
                    }
                default:
                    sender.sendMessage(Txt.parse(
                            "&8[&d&l*&8] &d&nItem Filter:\n" +
                                    "&7Ability to toggle what you're able to pickup from the ground\n" +
                                    "&7while &nPvPing&7 or &nGrinding&7.\n" +
                                    " \n" +
                                    "&8[&d&l*&8] &d&nCommands:&r\n" +
                                    " &d&l* &7/filter edit &d- &7Edit items you pickup while the filter is enabled.\n" +
                                    " &d&l* &7/filter toggle &d- &7Toggles the item filter on/off."
                    ));
                    return true;
            }
        }
        return false;
    }
}
